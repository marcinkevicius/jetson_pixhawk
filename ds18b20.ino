#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 9

OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);

 float Celcius=0;
 float Fahrenheit=0;
void setup(void)
{
  
  Serial.begin(115200);
  sensors.begin();
}

void loop(void)
{ 
  sensors.requestTemperatures();
  for (int i=0; i<32; i++){
    Celcius=sensors.getTempCByIndex(i);
    Serial.print(i);
    Serial.print(":");
    Serial.println(Celcius);
  }
  Serial.print("A0");
  Serial.print(":");
  Serial.println(analogRead(A0));
  Serial.print("A1");
  Serial.print(":");
  Serial.println(analogRead(A1));
  Serial.print("A2");
  Serial.print(":");
  Serial.println(analogRead(A2));
  Serial.print("A3");
  Serial.print(":");
  Serial.println(analogRead(A3));
  Serial.print("A4");
  Serial.print(":");
  Serial.println(analogRead(A4));
  Serial.print("A5");
  Serial.print(":");
  Serial.println(analogRead(A5));
  Serial.print("A6");
  Serial.print(":");
  Serial.println(analogRead(A6));
  Serial.print("A7");
  Serial.print(":");
  Serial.println(analogRead(A7));
  delay(1000);
}