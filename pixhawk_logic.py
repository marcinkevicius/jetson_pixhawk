#!/usr/bin/python3
from mavlink_api_communication import FlightProcedures
import sys
import serial.tools.list_ports
from collections import namedtuple
import struct
import datetime
import zmq
import json
import time
from threading import Thread, Event, Lock
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
import math
import pingparsing
import subprocess

mavlink_device_string = "tcp:192.168.100.241:5760"
# mavlink_device_string = "/dev/ttyTHS1"
zmq_server_string = "tcp://192.168.11.1:5555"
context = zmq.Context()
socket = context.socket(zmq.REP)
debug = 3                        # show debug info 0-no debug, 1-critical, 2 verbose, 3-everything
cycling = True
camera_angle = 0.0                  # camera pitch angle in deg

response_list = []
drone_commands = []             # drone commands to send to pix api
e_start_now = Event()           # information event to start sending drone commands immediately
e_start_now_camera_Master = Event()           # information event to start rotating main camera
reset_total_amps = False
terminate_all = False
transmit = False
network_state_msg = None            # msg from network state thread
fence_breach_msg = None             # msg from geofencing thread
flight_procedures = None

station_state = {'last_response': int(time.time() * 1000),
                 'gps': {'long': 0, 'lat': 0},
                 'state': 0  # state 0=ok, 2= sort a problem, but dont give a f*ck, 3=severe
                 }

pixhawk_state = {'last_response': int(time.time() * 1000),
                 'gps': {'long': 0, 'lat': 0, 'alt': 0},
                 'gps_timestamp': int(time.time() * 1000),
                 'gps_sats': {'gps_fix': 0, 'gps_num_sat': 0},
                 'gps_sats_timestamp': int(time.time() * 1000),
                 'state': 'disarmed',
                 'state_timestamp': int(time.time() * 1000),
                 'vehicle_mode': None,
                 'vehicle_mode_timestamp': int(time.time() * 1000),
                 'system_state': None,  # standby, None, active
                 'system_state_timestamp': int(time.time() * 1000),
                 'home_position': None,
                 'obey_rc_input': None,
                 'obey_rc_input_timestamp': int(time.time() * 1000),
                 'last_rc_input_timestamp': 0,
                 'watch_over_rc_inputs': False
                 }

system_state = {'high': {  #  high priority data - MUST BE OK
                    # 'drone': 0,  # 3 - no connection to pix controller, 0 - all good
                    'threads': {
                        'update_antenna_tracker': 0,
                        'send_drone_commands': 0,
                        'watch_rc_inputs': 0,
                        'voltage_data_read': 0,
                        'start_video_transmission': 0,
                        'current_data_read': 0,
                        'network_state_daemon': 0,
                        'geo_fencing': 0,
                        'camera_controll_Main': 0,
                        'station_connection': 0  #  watching if connection to station is good
                        }
                    }
                }

voltage_p_cell = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 'V': 0}
current = {'total': 0, 'current': 0}    # total , current draw in Amps

drone_variables = {
                    'ttyID': 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-2.2',   # ttyID battery usb interface
                    'pcm': {1: 2, 2: 3, 3: 5, 4: 4, 5: 7, 6: 6, 7: 0, 8: 1},  # pin_id->cell_id mapping
                    'voltage_p_point': {0: 3.82 / 793, 1: 7.64 / 790, 2: 11.47 / 765, 3: 15.3 / 777, 4: 19.14 / 666,
                                       5: 22.97 / 685, 6: 26.8 / 778, 7: 30.63 / 693},
                    'cell_count': 8,
                    'cell_min_v': -100,
                    'dummy_voltage': 0,      #get dummy voltage readings bypassing voltage sensor
                    'acs758': 'b',  # acs758 u- unidirectional, b- bidirectional
                    'ttyCameraIDMain': 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-2.1',
                    'maxCameraPitch': 90.0,
                    'minCameraPitch': -30.0,
                    'cameraOffset': -30,
                    'camFrameRollPitch': True,  #Fixed (to drone) axis roll, pitch axis is float
                    'reverseCameraPitch': False,
                    'camPort': '5001',
                    'camIP': '192.168.100.241',
                    'fenceAlt': 40,
                    'fenceRad': 5,
                    'allow_bat_ah': 0.1     # battery allowed limit in mAh, after witch drone goes into RTL mode. 0 - ignore
                                            # good idea to make 90% of your max (real) battery capacity
                   }
                # dummy_voltage : minutes -> set 3.7 for minutes time, then reduce to 3.6v. IF minutes 0, then disable dummy voltage


def lg(log_message, level):
    global response_list
    if level <= debug:
        print(datetime.datetime.now(), end='')
        if level == 1:
            print("[CRITICAL ]", end='')
            response_list.append({'error': {'rpi_sensors': log_message}})
        if level == 2:
            print("[IMPORTANT]", end='')
        if level == 3:
            print("[VERBOSE  ]", end='')
        if isinstance(log_message, list):
            print(">>", end='')
            for message in log_message:
                print("<>", end='')
                try:
                    print(message, end='')
                except:
                    print('none asci message')
            print("<<")
        else:
            try:
                print(log_message)
            except:
                print('none asci message')


def getserial():
    # Extract serial from cpuinfo file
    cpuserial = "0000000000000000"
    try:
        f = open('/proc/device-tree/serial-number', 'r')
        for line in f:
            cpuserial = line.rstrip('\x00')
            print(" serial no. %s " % cpuserial)
        f.close()
    except:
        cpuserial = "ERROR000000000"

    return cpuserial


def getDroneVariables():
    global drone_variables
    cpu_serial = getserial()
    if cpu_serial == "0000000012e71a9f":  # main (octo) drone unique ID
        print('Setting Octo Drone variables')
        drone_variables['ttyID'] = 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-2.2'
        drone_variables['ttyCameraIDMain'] = 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-2.1'
        drone_variables['reverseCameraPitch'] = True
        drone_variables['pcm'] = {1: 0, 2: 1, 3: 2, 4: 3, 5: 4, 6: 5, 7: 6, 8: 7}
        # drone_variables['voltage_p_point'] = {0: (5.0 / 1023), 1: 8.21 / 859, 2: 12.31 / 825, 3: 16.42 / 838,
        #                                       4: 20.4 / 729, 5: 24.5 / 738, 6: 28.6 / 748, 7: 32.7 / 754}
        drone_variables['voltage_p_point'] = {0: (4.88 / 1023), 1: 9.78 / 1023, 2: 15.25 / 1023, 3: 20.03 / 1023,
                                              4: 28.74 / 1023, 5: 34.00 / 1023, 6: 39.13 / 1023,
                                              7: 32.7 / 754}  # 5 - 24.8
        drone_variables['dummy_voltage'] = 10
        drone_variables['camFrameRollPitch'] = False
        drone_variables['maxCameraPitch'] = 30
        drone_variables['minCameraPitch'] = -90
        drone_variables['cameraOffset'] = -30

    elif cpu_serial == '1423619093270':  # test (hex) drone unique ID
        print('Setting test Drone variables')
        drone_variables['ttyID'] = 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-2.2'
        drone_variables['pcm'] = {1: 2, 2: 3, 3: 5, 4: 4, 5: 7, 6: 6, 7: 0, 8: 1}
        drone_variables['voltage_p_point'] = {0: 3.82 / 793, 1: 7.64 / 790, 2: 11.47 / 765, 3: 15.3 / 777,
                                              4: 19.14 / 666,
                                              5: 22.97 / 685, 6: 26.8 / 778, 7: 30.63 / 693}
        drone_variables['acs758'] = 'u'
        drone_variables['ttyCameraIDMain'] = 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-2.1'
        drone_variables['maxCameraPitch'] = 30
        drone_variables['minCameraPitch'] = -70
        drone_variables['dummy_voltage'] = 10
        drone_variables['allow_bat_ah'] = 3.0  # left - 4020 ;right - 3820
        drone_variables['cameraOffset'] = 0
    else:
        print("no valid serial found %s" % cpu_serial)


def set_camera_pitch_angle(pwm_data):  # rc_1234 c45 1266 1899 ...
    this_t = 'set_camera_pitch_angle'
    global camera_angle
    rc_channels = pwm_data.split(' ')
    rc_channels.remove('rc_1234')
    lg([this_t, rc_channels], 3)
    for rc_channel in rc_channels:
        if rc_channel.startswith('c'):  # if roll -> 1
            try:
                camera_angle = float(rc_channel[1:])
            except:
                lg([this_t, 'Wrong camera angle!!!:', rc_channel[1:]], 1)
                camera_angle = 0
            if drone_variables['reverseCameraPitch']:
                camera_angle = camera_angle * -1
            if camera_angle > drone_variables['maxCameraPitch']:
                camera_angle = drone_variables['maxCameraPitch']
            if camera_angle < drone_variables['minCameraPitch']:
                camera_angle = drone_variables['minCameraPitch']
            camera_angle += drone_variables['cameraOffset']
            lg([this_t, "camera_angle " + str(camera_angle)], 2)
            e_start_now_camera_Master.set()


###################### START ######################
######## camera controll movements  ###############
###################################################

CMD_CONTROL = 67
SBGC_CONTROL_MODE_NO = 0
SBGC_CONTROL_MODE_SPEED = 1
SBGC_CONTROL_MODE_ANGLE = 2
SBGC_CONTROL_MODE_SPEED_ANGLE = 3
SBGC_CONTROL_MODE_RC = 4
SBGC_CONTROL_MODE_ANGLE_REL_FRAME = 5
SBGC_SPEED_SCALE = float(1.0 / 0.1220740379)
SBGC_ANGLE_FULL_TURN = 16384
SBGC_DEGREE_ANGLE_SCALE = float(SBGC_ANGLE_FULL_TURN / 360.0)

ControlData = namedtuple(
    'ControlData',
    'controll_mode roll_speed roll_angle pitch_speed pitch_angle yaw_speed yaw_angle')

Message = namedtuple(
    'Message',
    'start_character command_id payload_size header_checksum payload payload_checksum')


def pack_control_data(control_data):
    print('controll_dataaaaa')
    print(control_data)
    return struct.pack('<Bhhhhhh', *control_data)


def create_message(command_id, payload):
    payload_size = len(payload)
    return Message(start_character=ord('>'),command_id=command_id,
                   payload_size=payload_size,
                   header_checksum=(command_id + payload_size) % 256,
                   payload=payload,
                   payload_checksum=(sum(bytearray(payload)) % 256))


def pack_message(message):
    message_format = '<BBBB{}sB'.format(message.payload_size)
    return struct.pack(message_format, *message)


def SBGC_DEGREE_TO_ANGLE(degree):
    return int(degree * SBGC_DEGREE_ANGLE_SCALE)


def camera_angle_controll_Main(e_start_now_camera_Master):
    this_t = 'camera_angle_controll_Main'

    if drone_variables['ttyCameraIDMain'] is None:
        print('camera Main serial is not set')
        return

    ttycamera=None

    for port in serial.tools.list_ports.comports():
        if drone_variables['ttyCameraIDMain'] == port.hwid:
            ttycamera = port.device

    if ttycamera is None:
        print('No camera Main serial device found')
        return

    global old_angle
    connection = serial.Serial(ttycamera, baudrate=115200, timeout=10)
    old_angle = camera_angle

    while not terminate_all:
        system_state['high']['threads']['camera_controll_Main'] = int(time.time() * 1000)
        e_start_now_camera_Master.wait(1)
        e_start_now_camera_Master.clear()
        if old_angle != camera_angle:
        # if True:
            old_angle = camera_angle

            if drone_variables['camFrameRollPitch']:
                control_data = ControlData(controll_mode=SBGC_CONTROL_MODE_ANGLE,
                                           roll_speed=int(100 * SBGC_SPEED_SCALE), roll_angle=SBGC_DEGREE_TO_ANGLE(0),
                                           pitch_speed=int(100 * SBGC_SPEED_SCALE), pitch_angle=SBGC_DEGREE_TO_ANGLE(old_angle),
                                           yaw_speed=int(100 * SBGC_SPEED_SCALE), yaw_angle=0)
            else:
                control_data = ControlData(controll_mode=SBGC_CONTROL_MODE_ANGLE,
                                           roll_speed=int(100 * SBGC_SPEED_SCALE), roll_angle=SBGC_DEGREE_TO_ANGLE(old_angle),
                                           pitch_speed=int(100 * SBGC_SPEED_SCALE), pitch_angle=SBGC_DEGREE_TO_ANGLE(0),
                                           yaw_speed=int(100 * SBGC_SPEED_SCALE), yaw_angle=0)

            packed_control_data = pack_control_data(control_data)
            message_data = create_message(CMD_CONTROL, packed_control_data)
            packed_message = pack_message(message_data)
            connection.write(packed_message)
            lg([this_t, 'writing to camera Main'], 3)
        time.sleep(1)

###################################################
######## camera controll movements  ###############
###################   END   #######################


# reply all data to Statio. All data gathered in response list
# and ready to be sent to station
def reply_all_data():  # this one replies all data to station
    this_t = 'reply_all_data'
    global response_list
    global pixhawk_state
    # always reply voltage and amperage
    if len(response_list) > 100:
        response_list.clear()

    response_list.append({'voltage': voltage_p_cell})  #appending critical data
    response_list.append({'current': current})  # appending critical data
    response_list.append({'pixhawk_state': pixhawk_state})

    lg([this_t, "replaying To station", response_list], 3)

    response_list_send = list(filter(None, response_list))
    response_list.clear()

    lg([this_t, "MSG -> station: ", response_list_send], 2)

    socket.send_json(json.dumps(response_list_send))
    lg([this_t, "all data replied to ZMQ server"], 3)


def station_connection():
    this_t = 'station_connection'

    while not terminate_all:
        delta_time = int(int(time.time() * 1000) - station_state['last_response'])

        # if pixhawk is not armed, or station response below 2s, then all good
        if (pixhawk_state['system_state'] in ["standby", None]) or delta_time < 2000:
            station_state['state'] = 0  # ok
        elif delta_time > 20000:
            station_state['state'] = 3  # severe
            lg([this_t, "connection to station lost for more than 20s !!!"], 1)
        else:
            lg([this_t, "connection to station lost for more than 2s !!!"], 1)
            station_state['state'] = 1  # sort a problem, but dont give a f*ck

        time.sleep(0.5)
        system_state['high']['threads']['station_connection'] = int(time.time() * 1000)

    lg([this_t, "THREAD station_connection done "], 2)


def set_drone_mode_state(set_state, flight_procedures):
    global pixhawk_state
    return_val = None
    time_now = int(time.time() * 1000)
    this_t = 'set_drone_mode_state'
    if set_state == 'guided':
        lg([this_t, 'PIX REQUEST set mode', set_state], 3)
        return_val = flight_procedures.guided()
        pixhawk_state['last_response'] = time_now
        pixhawk_state['vehicle_mode'] = 'guided'
        pixhawk_state['vehicle_mode_timestamp'] = time_now

    elif set_state == 'rtl':
        lg([this_t, 'PIX REQUEST set mode', set_state], 3)
        return_val = flight_procedures.go_home()
        pixhawk_state['last_response'] = time_now
        pixhawk_state['vehicle_mode'] = 'rtl'
        pixhawk_state['vehicle_mode_timestamp'] = time_now

    elif set_state == 'land':
        lg([this_t, 'PIX REQUEST set mode', set_state], 3)
        return_val = flight_procedures.land()
        pixhawk_state['last_response'] = time_now
        pixhawk_state['vehicle_mode'] = 'land'
        pixhawk_state['vehicle_mode_timestamp'] = time_now

    elif set_state == 'poshold':
        lg([this_t, 'PIX REQUEST set mode', set_state], 3)
        return_val = flight_procedures.poshold()
        pixhawk_state['last_response'] = time_now
        pixhawk_state['vehicle_mode'] = 'poshold'
        pixhawk_state['vehicle_mode_timestamp'] = time_now

    elif set_state == 'loiter':
        lg([this_t, 'PIX REQUEST set mode', set_state], 3)
        return_val = flight_procedures.loiter()
        pixhawk_state['last_response'] = time_now
        pixhawk_state['vehicle_mode'] = 'loiter'
        pixhawk_state['vehicle_mode_timestamp'] = time_now

    elif set_state == 'stab' and pixhawk_state['obey_rc_input']:
        lg([this_t, 'PIX REQUEST set mode', set_state], 3)
        return_val = flight_procedures.stabilize()
        pixhawk_state['last_response'] = time_now
        pixhawk_state['vehicle_mode'] = 'stab'
        pixhawk_state['vehicle_mode_timestamp'] = time_now

    return return_val


def send_drone_commands(e_start_now):
    global flight_procedures
    global drone_commands
    global pixhawk_state
    global response_list
    clean_channels = {'1': None, '2': None, '3': None, '4': None }  # roll, pitch, throttle, yaw
    default_channels = {'1': 1500, '2': 1500, '3': 1500, '4': 1500 }  # roll, pitch, throttle, yaw
    this_t = 'send_drone_commands'
    lg([this_t, "connecting to pixhawk controller"], 2)
    flight_procedures = FlightProcedures(mavlink_device_string)

    while not terminate_all:
        while drone_commands:
            lg([this_t, "total commands", drone_commands], 3)
            time_now = int(time.time() * 1000)
            system_state['high']['threads']['send_drone_commands'] = time_now
            drone_exec = drone_commands[0]
            drone_commands.remove(drone_exec)
            lg([this_t, "new drone command request " + drone_exec], 3)

            if time_now - pixhawk_state['gps_timestamp'] > 1300:
                pixhawk_state['gps'] = flight_procedures.get_position()
                lg([this_t, 'PIX REQUEST', 'innerloop, getting position'], 3)
                pixhawk_state['last_response'] = time_now
                pixhawk_state['gps_timestamp'] = time_now

            if time_now - pixhawk_state['vehicle_mode_timestamp'] > 2500:
                lg([this_t, 'PIX REQUEST', 'innerloop getting pix status'], 3)
                tmp_pixhawk_state = flight_procedures.get_status()
                pixhawk_state['last_response'] = time_now

                pixhawk_state['vehicle_mode'] = tmp_pixhawk_state['vehicle_mode']
                pixhawk_state['vehicle_mode_timestamp'] = time_now

                pixhawk_state['gps_sats']['gps_fix'] = tmp_pixhawk_state['gps_fix']
                pixhawk_state['gps_sats']['gps_num_sat'] = tmp_pixhawk_state['gps_num_sat']
                pixhawk_state['gps_sats_timestamp'] = time_now

                pixhawk_state['state'] = tmp_pixhawk_state['arm_msg']
                pixhawk_state['state_timestamp'] = time_now

                pixhawk_state['system_state'] = tmp_pixhawk_state['system_state']
                pixhawk_state['system_state_timestamp'] = time_now

            if drone_exec == 'go_home_position':  # returns to home position but never lands
                if pixhawk_state['home_position'] is not None:
                    lg([this_t, 'PIX REQUEST', drone_exec], 3)
                    response_list.append(flight_procedures.go_home_position(pixhawk_state['home_position'], 5))
                    pixhawk_state['last_response'] = time_now
                    response_list.append(set_drone_mode_state('guided', flight_procedures))
                    response_list.append({"vehicle_mode": "guided"})

            if drone_exec == 'arm':
                pixhawk_state['home_position'] = flight_procedures.get_home_position()
                pixhawk_state['last_response'] = time_now
                if pixhawk_state['home_position'] is not None:
                    # always arm in guided mode
                    response_list.append(set_drone_mode_state('guided', flight_procedures))
                    lg([this_t, 'PIX REQUEST', drone_exec], 3)
                    response_list.append(flight_procedures.arm())
                    pixhawk_state['last_response'] = time_now

                    # home position chages after arming, so get new
                    lg([this_t, 'updating home_position', pixhawk_state['home_position']], 3)
                    lg([this_t, 'PIX REQUEST', 'get_home_position'], 3)
                    pixhawk_state['home_position'] = flight_procedures.get_home_position()
                    pixhawk_state['last_response'] = time_now
                    response_list.append({'home_position': pixhawk_state['home_position']})
                else:
                    lg([this_t, 'cant arm, home position is NONE'], 1)

            if drone_exec == 'take_off':
                take_off_altitude = 5
                tmp_msg = flight_procedures.take_off(take_off_altitude)  # take off to 5m from ground
                lg([this_t, 'PIX REQUEST', drone_exec], 3)
                pixhawk_state['last_response'] = time_now
                response_list.append(tmp_msg)
                if "doing_state" in tmp_msg:  # if got state and no errors
                    if 'taking_off' == tmp_msg['doing_state']:  # if taking of
                        for x in range(1, (take_off_altitude + 1)):  # respond about altitude every "take_off_altitude" second
                            response_list.append(flight_procedures.get_altitude())
                            lg([this_t, 'PIX REQUEST', 'update altitude'], 3)
                            time_now = int(time.time() * 1000)
                            pixhawk_state['last_response'] = time_now
                            system_state['high']['threads']['send_drone_commands'] = time_now
                            time.sleep(1)

            if drone_exec == 'disarm':
                tmp_pixhawk_state = flight_procedures.disarm()
                lg([this_t, 'PIX REQUEST', drone_exec], 3)
                pixhawk_state['last_response'] = time_now
                pixhawk_state['state'] = tmp_pixhawk_state['arm_msg']
                pixhawk_state['state_timestamp'] = time_now
                response_list.append(tmp_pixhawk_state)

            if drone_exec == 'man':
                response_list.append(set_drone_mode_state('guided', flight_procedures))
                tmp_channels = flight_procedures.set_rc_1234("rc_1234", clean_channels, True)
                lg([this_t, 'PIX REQUEST', drone_exec], 3)
                pixhawk_state['last_response'] = time_now
                pixhawk_state['obey_rc_input'] = True
                pixhawk_state['obey_rc_input_timestamp'] = time_now

                # do failsafe chack on throttle (and other manual rc input)
                if 1300 < tmp_channels['rc_channels']['throttle'] < 1700 and \
                        1400 < tmp_channels['rc_channels']['roll'] < 1600 and \
                        1400 < tmp_channels['rc_channels']['pitch'] < 1600 and \
                        1400 < tmp_channels['rc_channels']['yaw'] < 1600:
                    # if sticks are in the middle
                    response_list.append(tmp_channels)
                else:
                    lg([this_t, 'Sticks are not in the middle. Switching to digital mode. sticks:',tmp_channels], 1)
                    response_list.append(flight_procedures.set_rc_1234("rc_1234"))
                    lg([this_t, 'PIX REQUEST', 'rc_1234'], 3)
                    pixhawk_state['last_response'] = time_now
                    pixhawk_state['obey_rc_input'] = False
                    pixhawk_state['obey_rc_input_timestamp'] = time_now
                response_list.append({'obey_rc_input': pixhawk_state['obey_rc_input']})

            if drone_exec == 'dig':
                response_list.append(set_drone_mode_state('guided', flight_procedures))
                pixhawk_state['obey_rc_input'] = False
                pixhawk_state['obey_rc_input_timestamp'] = time_now
                response_list.append(flight_procedures.set_rc_1234("rc_1234"))
                lg([this_t, 'PIX REQUEST', drone_exec], 3)
                pixhawk_state['last_response'] = time_now
                response_list.append({'obey_rc_input': pixhawk_state['obey_rc_input']})

            if drone_exec == 'get_rc_ch':
                response_list.append(flight_procedures.get_rc_ch())
                pixhawk_state['last_response'] = time_now
                response_list.append({'obey_rc_input': pixhawk_state['obey_rc_input']})

            if drone_exec.startswith('rc_1234'):
                pixhawk_state['last_rc_input_timestamp'] = time_now  # register rc imput time stamp
                pixhawk_state['watch_over_rc_inputs'] = True

                if not pixhawk_state['obey_rc_input']:  # obey radio rc input over digital
                    lg([this_t, 'PIX REQUEST', drone_exec], 3)

                    # rc dig or man inputs can interfere on landing and rtl modes
                    if pixhawk_state['vehicle_mode'] not in ['rtl', 'land']:
                        response_list.append(flight_procedures.set_rc_1234(drone_exec))
                        pixhawk_state['last_response'] = time_now
                    else:
                        lg([this_t, 'setting default rc inputs, mode in rtl or land'], 3)
                        response_list.append(flight_procedures.set_rc_1234(drone_exec, default_channels))
                        pixhawk_state['last_response'] = time_now
                else:
                    response_list.append(flight_procedures.set_rc_1234(drone_exec, clean_channels))
                    pixhawk_state['last_response'] = time_now

            if drone_exec in ['guided', 'stab', 'land', 'rtl']:
                response_list.append(set_drone_mode_state(drone_exec, flight_procedures))
                lg([this_t, 'PIX REQUEST', drone_exec], 3)

            if drone_exec in ['poshold', 'loiter']:
                response_list.append(set_drone_mode_state(drone_exec, flight_procedures))
                response_list.append(flight_procedures.set_rc_1234("rc_1234"))
                lg([this_t, 'PIX REQUEST', drone_exec], 3)
                pixhawk_state['last_response'] = time_now

            if drone_exec == 'get_home_position' and pixhawk_state['home_position'] is None:
                pixhawk_state['home_position'] = flight_procedures.get_home_position()

            if drone_exec in ['get_drone_gps', 'get_altitude']:
                response_list.append(pixhawk_state['gps'])
            if drone_exec in ['get_state', 'get_home_position']:
                response_list.append({'pixhawk_state': pixhawk_state})

            # time.sleep(0.02)

        lg([this_t, "no new drone command request"], 3)
        e_start_now.wait(0.2)  # sleep for .5s or until we get event command
        e_start_now.clear()  # clearing event for new listenning
        system_state['high']['threads']['send_drone_commands'] = int(time.time() * 1000)

        # no one requested data? check if pixhawk is alive by sending request
        if int(time.time() * 1000) - pixhawk_state['last_response'] > 2000:
            drone_commands.append('get_drone_gps')  # gps is the most updated variable
        if int(time.time() * 1000) - pixhawk_state['last_response'] > 2200:
            lg([this_t, 'last pixhawk Responce >2.2s, PIXHAWK NOT RESPONDING'], 1)

    flight_procedures.go_home()
    lg([this_t, 'PIX REQUEST', 'go_home final request'], 3)
    pixhawk_state['last_response'] = time_now
    time.sleep(1)
    system_state['high']['threads']['send_drone_commands'] = int(time.time() * 1000)
    flight_procedures.close_connection()
    lg([this_t, 'PIX REQUEST', 'close connection'], 3)
    pixhawk_state['last_response'] = time_now
    time.sleep(0.3)
    lg([this_t, "THREAD send_drone_commands done"], 2)


def watch_rc_inputs():
    this_t = 'watch_rc_inputs'
    global drone_commands
    global pixhawk_state

    while not terminate_all:

        wait_for_data = False

        time_now = int(time.time() * 1000)

        # check if watching input state is enabled
        if pixhawk_state['watch_over_rc_inputs']:

            if (time_now - pixhawk_state['last_rc_input_timestamp']) > 2000:  # if more than 2s without rc input

                if (time_now - pixhawk_state['gps_timestamp']) > 2000:  # if altitude is registered within 2s
                    drone_commands.append('get_position')
                    wait_for_data = True

                if (time_now - pixhawk_state['state_timestamp']) > 3000:  # if last state is registered within 3s
                    drone_commands.append('get_state')
                    wait_for_data = True

                if wait_for_data:  # we asked pix state or position so wait for response
                    system_state['high']['threads']['watch_rc_inputs'] = time_now  #update time stamp just in case
                    time.sleep(1)
                    time_now = int(time.time() * 1000)
                    system_state['high']['threads']['watch_rc_inputs'] = time_now

                # if drone is in the air and in LOITER/POSHOLD manual controll mode
                if pixhawk_state['gps']['alt'] > 2.0 and pixhawk_state['vehicle_mode'] in ['loiter', 'poshold']:

                    # just check for data state and register logs
                    if time_now - pixhawk_state['gps_timestamp'] > 2000 and \
                       time_now - pixhawk_state['vehicle_mode_timestamp'] > 2500:
                        lg([this_t, 'doing decisions on OLD vehicle mode or GPS data!!!'], 1)

                    # if no rc input data < 2min
                    if (time_now - pixhawk_state['last_rc_input_timestamp']) < 120000:  # if less than 2min

                        # append loiter or poshold (rc channels are automatically set to default 'rc_1234')
                        drone_commands.append(pixhawk_state['vehicle_mode'])
                        lg([this_t, 'no rc_input for > 2s, seting default RC channels, mode: ' +
                            pixhawk_state['vehicle_mode']], 1)

                    else:
                        # no imput for more than 2min
                        # somethings wrong with rc input, set go home location and turn off rc input watching
                        drone_commands.append('rtl')
                        pixhawk_state['watch_over_rc_inputs'] = False
                        lg([this_t, 'FAILSAFE[watch_rc_inputs]: no rc_input for > 2min, Going RTL'], 1)

        system_state['high']['threads']['watch_rc_inputs'] = time_now
        time.sleep(1)


# start drone dummy voltage readings for dummy_voltage minutes, then sets cell voltages to low(3.6volts)
def voltage_data_read_dummy():
    global voltage_p_cell
    start_time = time.time()
    set_low_after_sec = drone_variables['dummy_voltage'] * 60
    voltage_p_cell = {0: 4.2, 1: 4.0, 2: 3.9, 3: 3.8, 4: 3.7, 5: 3.7, 6: 3.7, 7: 3.7, 'V': 32.2}
    while not terminate_all:
        system_state['high']['threads']['voltage_data_read'] = int(time.time() * 1000)
        time.sleep(1)
        end_time = time.time()
        # lg(['time elapsed in s:',end_time - start_time,'time threshold in s',set_low_after_sec], 1)
        if (end_time - start_time) > set_low_after_sec:
            voltage_p_cell = {0: 3.8, 1: 3.7, 2: 3.6, 3: 3.6, 4: 3.6, 5: 3.6, 6: 3.6, 7: 3.6, 'V': 28.8}


def voltage_data_read():
    """
    # cell  - pin   - 5.13v - voltage/point mappting
    # c0    - pin7  - 1024	- 3.82 = 793
    # c1    - pin8  - 528   - ...
    # c2    - pin1  - 340   - ...
    # c3    - pin2  - 258   - ...
    # c4    - pin4  - 176   - ...
    # c5    - pin3  - 151   - ...
    # c6    - pin6  - 146   - ...
    # c7    - pin5  - 113   - ...
    :return:
    """
    # pcm - pin cell mapping
    # pcm = {0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7} [pin_id]->[cell_id]
    # if drone_cell_pin_mapping == "pix":
    #     pcm = {1: 2, 2: 3, 3: 5, 4: 4, 5: 7, 6: 6, 7: 0, 8: 1}
    # else:
    #     if drone_cell_pin_mapping == "":
    #         lg("THREAD voltage_data_read disabled ", 2)
    #         return

    # voltage_p_point = {0:3.82 / 793, 1:7.64 / 790, 2:11.47 / 765, 3: 15.3 / 777, 4: 19.14 / 666, 5: 22.97 / 685, 6: 26.8 / 778, 7: 30.63 / 693}

    this_t = 'voltage_data_read'
    pcm = drone_variables['pcm']

    lg([this_t, "THREAD voltage_data_read started "], 2)

    global voltage_p_cell
    analog_outputs = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0}
    voltage_p_point = drone_variables['voltage_p_point']
    true_voltage = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0}

    if drone_variables['ttyID'] is None:
        lg([this_t, "Battery serial is not set"], 1)
        return

    ttybattery = None

    for port in serial.tools.list_ports.comports():
        if drone_variables['ttyID'] == port.hwid:
            ttybattery = port.device

    if ttybattery is None:
        lg([this_t, 'No Battery serial device found'], 1)
        return

    port = serial.Serial(ttybattery, baudrate=4800, timeout=3.0)

    while not terminate_all:
        data = port.readline().decode().split(":")
        try:  # checking if data is readable
            data[0] = int(data[0])
            lg([this_t, 'voltage_data_read', data[0], int(data[1])], 3)
        except:
            data[0] = 10  # make dummy data

        if 1 <= data[0] <= 8:
            if True:
                lg([this_t, "cell :" + str(pcm[data[0]])], 3)
                analog_outputs[pcm[data[0]]] = int(data[1])  # wite analog data by cell to analog_outputs array
                lg([this_t, "analog_outputs :"+str(analog_outputs[pcm[data[0]]])], 3)
                true_voltage[pcm[data[0]]] = analog_outputs[pcm[data[0]]] * voltage_p_point[pcm[data[0]]]  #convert analog data to voltage
                lg([this_t, 'voltage_data_read',"true_voltage :" + str(true_voltage[pcm[data[0]]])], 3)
                if pcm[data[0]] > 0:  # if cell is not first cell
                    #  recalculate individual voltage by cell
                    voltage_p_cell[pcm[data[0]]] = round(true_voltage[pcm[data[0]]] - true_voltage[(pcm[data[0]]-1)], 3)
                    if (drone_variables['cell_count'] - 1) == pcm[data[0]]:  # if this is the last cell - get whole voltage of a battery
                        voltage_p_cell['V'] = round(true_voltage[pcm[data[0]]], 3)
                else:
                    voltage_p_cell[pcm[data[0]]] = round(true_voltage[pcm[data[0]]], 3)

                lg([this_t, "voltage_p_cell :" + str(voltage_p_cell[pcm[data[0]]])], 3)
        system_state['high']['threads']['voltage_data_read'] = int(time.time() * 1000)

    port.close()
    lg([this_t, "THREAD voltage_data_read done"], 2)


def current_data_read():
    this_t = 'current_data_read'
    global current
    global reset_total_amps
    file_path = '../last_amperage_data'
    i2c = busio.I2C(board.SCL, board.SDA)
    ads = ADS.ADS1115(i2c, gain=2/3, data_rate=860)

    if drone_variables['acs758'] == 'u':
        adc_offset = -1  # adc offset 0v = -3
        acs758_offset = 3320  # 0.600 # ACS offset ~.6V
        scale_factor = 0.04  # mV per Amp
        V1 = 6.144  # adc max voltage by factory
        N1 = 32767  # adc max raw voltage reading on V1 by factory
        final_adjustment = 1.0  # final amperage is off by ~10%
        apb = (V1 / N1) / scale_factor  # Amp per 1 Bit
        apb = apb / final_adjustment
    else:
        # device specs: 32767 - 6.144
        scale_factor = 0.018  # 20mV per Amp
        adc_offset = 0
        V1 = 0.0002  # V1 = 6.144  # max adc voltage calculated on V1
        N1 = 1  # N1 = 32767  # max adc raw voltage reading on V1
        acs758_offset = 13730  # ACS offset 2.5737035432 V - 13726
        final_adjustment = 1
        apb = (V1 / N1) / scale_factor  # Amp per Bit
        apb = apb / final_adjustment


        #  10.39A/825 = 0.01259393939A/acs_digit
        # 0.00020/0.016 = 0.0125  = apb
        # apb = 0.0110

    total_amps = 0

    try:
        f = open(file_path, "r")
        total_amps_saved = float(f.read())
        f.close()
    except IOError:
        total_amps_saved = 0
    except ValueError:
        total_amps_saved = 0

    lg([this_t, "THREAD current_data_read started "], 2)
    lg([this_t, "total_amps "+str(total_amps_saved)], 2)
    while not terminate_all:
        samples_count = 0
        samples_sum = 0
        start = time.time()
        system_state['high']['threads']['current_data_read'] = int(start * 1000)
        while (time.time() - start) <= 1.0:
            value = ads.read(0) + adc_offset
            #lg([this_t, 'values', value], 3)

            time.sleep(0.005)
            samples_sum += value
            samples_count += 1

        current_amps = ((samples_sum / samples_count) - acs758_offset) * apb
        total_amps += (current_amps / 3600.0)
        current = {'total': (total_amps + total_amps_saved), 'current': current_amps}

        if reset_total_amps:
            total_amps = 0.0
            total_amps_saved = 0.0
            reset_total_amps = False
        lg([this_t, 'amperage :'+str(current_amps)+' total ah :'+str(current['total'])], 3)

    f = open(file_path, "w+")
    f.write(str(current['total']))
    f.close()

    lg([this_t, "THREAD current_data_read done"], 2)


def start_video_transmission():
    this_t = 'start_video_transmission'
    global transmit
    # try:
    #     os.chdir(sys.argv[1])  # go to video scripts directory
    # except:
    #     lg([this_t, "Please pass video scripts directory_name", sys.argv[1]], 1)
    #     # exit(0)
    while not terminate_all:
        system_state['high']['threads']['start_video_transmission'] = int(time.time() * 1000)
        if transmit:
            lg([this_t, "starting video transmission"], 2)
            process = subprocess.Popen(['/usr/src/jetson_multimedia_api/samples/133_multicam_enc/multi_camera',
                                        drone_variables['camIP'], drone_variables['camPort']],
                             stdout=subprocess.DEVNULL,
                             universal_newlines=True)
            a_pid_code = process.poll()
            while a_pid_code is None:
                lg([this_t, 'still sending video to video server'], 3)
                time.sleep(1)
                a_pid_code = process.poll()
            lg([this_t, "start video transmission terminated with code", a_pid_code], 1)
            transmit = False
        #     a_pid = Popen(["./a.out", drone_variables['camIP'], drone_variables['camPort']])
        #     time.sleep(0.5)  # wait for connection
        #     a_pid_code = a_pid.poll()  # get process status
        #     if a_pid_code is None:  # checking if connection is made and no error code received
        #         lg([this_t, "starting video transmission 2/2"], 2)
        #         # rpivid = Popen(["/usr/bin/raspivid", "-t", "0", "-w", "1296", "-h", "972",
        #         #                 "-fps", "49", "-b", "5000000", "-o", "bufferin"])
        #         rpivid = Popen(["/usr/bin/raspivid", "-t", "0", "-w", "960", "-h", "960",
        #                         "-fps", "42", "-b", "20000000", "-sh", "50", "-o", "bufferin"])
        #         # "-fps", "42", "-b", "20000000", "-sh", "50", "-o", "bufferin"])
        #         a_pid.wait()
        #
        #         rpivid.terminate()
        #
        #     else:
        #         lg([this_t, "start video transmission terminated", a_pid_code], 1)
        #
        #     transmit = False
        time.sleep(1)
    lg([this_t, "THREAD start_video_transmission done"], 2)


# geofence calculations on loiter, poshold, stabilize, althold modes
def geo_fencing():
    this_t = 'geo_fencing'
    global system_state
    global fence_breach_msg
    while not terminate_all:
        system_state['high']['threads']['geo_fencing'] = int(time.time() * 1000)

        if pixhawk_state['home_position'] is not None:  # request home location data if not set
            # check if all variables are set and drone in control (loiter, poshold) mode
            if pixhawk_state['gps']['long'] != 0 and \
                    pixhawk_state['vehicle_mode'] in ['loiter', 'poshold', 'stab', 'althold']:
                lg([this_t,'Calculating fence breach', pixhawk_state['home_position'], pixhawk_state['gps']], 3)
                # lg(pixhawk_state['home_position'], 3)
                # lg(pixhawk_state['gps'], 3)

                ext_lat = math.radians(pixhawk_state['gps']['lat'])
                ext_lon = math.radians(pixhawk_state['gps']['long'])
                int_lat = math.radians(pixhawk_state['home_position']['lat'])
                int_lon = math.radians(pixhawk_state['home_position']['long'])

                dlon = int_lon - ext_lon
                dlat = int_lat - ext_lat

                p1 = dlon * math.cos((int_lat + ext_lat) / 2)
                p2 = dlat
                distance = 6371000 * math.sqrt(p1 * p1 + p2 * p2)  # distance in meter
                fence_breach_msg = None
                lg([this_t,'fence distance: ' + str(distance)], 3)

                if distance > drone_variables['fenceRad']:
                    fence_breach_msg = 'Fence breach!!! too FAR: ' + str(distance)+" > " + str(drone_variables['fenceRad'])
                    # lg('Fence breach!!! too FAR: ' + str(distance)+" > " + str(drone_variables['fenceRad']), 1)
                elif pixhawk_state['gps']['alt'] < 2.0:
                    fence_breach_msg = 'Fence breach!!! too LOW: ' + str(pixhawk_state['gps']['alt']) + " < 2m"
                elif pixhawk_state['gps']['alt'] > drone_variables['fenceAlt']:
                    fence_breach_msg = 'Fence breach!!! too HIGH: ' + str(pixhawk_state['gps']['alt']) + " > " + str(
                        drone_variables['fenceAlt'])
            else:
                fence_breach_msg = None

        else:
            lg([this_t,'No Home position set, requesting Home position ...'], 3)
            drone_commands.append('get_home_position')
        time.sleep(1)

    time.sleep(1)


def network_state_daemon():
    this_t = 'network_state_daemon'
    global network_state_msg

    ping_parser = pingparsing.PingParsing()
    transmitter = pingparsing.PingTransmitter()
    transmitter.destination = "192.168.11.1"
    transmitter.count = 5
    transmitter.packet_size = 1400
    transmitter.timeout = 1
    transmitter.deadline = 10
    transmitter.ping_option = "-l 5"

    while not terminate_all:
        system_state['high']['threads']['network_state_daemon'] = int(time.time() * 1000)
        result = transmitter.ping()
        ping_stats = ping_parser.parse(result).as_dict()

        # "rtt_avg": 2.075,
        # "packet_transmit": 5,
        # "packet_duplicate_rate": 0.0,
        # "rtt_max": 2.357,
        # "packet_duplicate_count": 0,
        # "packet_loss_count": 0,
        # "packet_loss_rate": 0.0,
        # "rtt_min": 1.808,
        # "destination": "192.168.11.1",
        # "packet_receive": 5,
        # "rtt_mdev": 0.229

        if ping_stats['packet_loss_count'] or ping_stats['rtt_avg'] > 1000.0:
            network_state_msg = "communication link unstable, loosing " + str(ping_stats['packet_loss_count']) + \
                                " packets out of " + str(ping_stats['packet_transmit']) + ", avg delay " + \
                                str(ping_stats['rtt_avg']) + "ms."
            lg([this_t, network_state_msg], 1)
        else:
            lg([this_t, ping_stats], 3)
            network_state_msg = None
        for s in range(5):
            system_state['high']['threads']['network_state_daemon'] = int(time.time() * 1000)
            time.sleep(1)


def system_error_state_handler():
    this_t = 'system_error_state_handler'

    lg([this_t, 'starting system_error_state_handler process in 3s'], 3)
    time.sleep(3)
    global fence_breach_msg
    global network_state_msg
    global flight_procedures

    command_send_time = int(time.time() * 1000) - 20000

    while not terminate_all:
        # 0 good (do nothing);
        # 1 fixable (return home pos);
        # 2 severe (TRL);
        # 3 critical (land NOW!)
        system_state_severity = 0
        current_time = int(time.time() * 1000)

        if current_time - system_state['high']['threads']['start_video_transmission'] > 2000 and not transmit:
            system_state_severity = 0
            lg([this_t, "thread start_video_transmission is dead"], 1)

        if current_time - system_state['high']['threads']['camera_controll_Main'] > 2000:
            system_state_severity = 1
            lg([this_t, "thread Main camera controll is dead"], 1)

        # not implemented at this time
        # if current_time - system_state['high']['threads']['update_antenna_tracker'] > 2000:
        #     system_state_severity = 1
        #     lg([this_t, "thread update_antenna_tracker is dead"], 1)

        if current_time - system_state['high']['threads']['network_state_daemon'] > 2000:
            system_state_severity = 1
            lg([this_t, "thread network_state_daemon is dead"], 1)
        else:
            if network_state_msg is not None and \
                    pixhawk_state["vehicle_mode"] not in ['rtl', 'land'] and \
                    pixhawk_state['system_state'] not in ["standby", None]:  # if network glitching is registered
                system_state_severity = 1
                lg([this_t, network_state_msg], 1)

        if current_time - system_state['high']['threads']['geo_fencing'] > 2000:
            system_state_severity = 1
            lg([this_t, "thread geo_fencing is dead"], 1)
        else:
            if fence_breach_msg is not None and \
                    pixhawk_state["vehicle_mode"] not in ['rtl', 'land'] and \
                    pixhawk_state['system_state'] not in ["standby", None]:  # if fence breach is registered
                system_state_severity = 1
                lg([this_t, fence_breach_msg], 1)

        if current_time - system_state['high']['threads']['station_connection'] > 2000 or station_state['state'] == 3:
            system_state_severity = 2
            lg([this_t, "thread station_connection is dead"], 1)

        if current_time - system_state['high']['threads']['watch_rc_inputs'] > 3000:
            lg([this_t, "thread watch_rc_inputs is dead !!!"], 1)
            system_state_severity = 2

        if current_time - system_state['high']['threads']['voltage_data_read'] > 2000:
            system_state_severity = 3
            lg([this_t, "thread voltage_data_read is dead"], 1)
        else:
            for cell_id in range(drone_variables['cell_count']):
                if voltage_p_cell[cell_id] < drone_variables['cell_min_v']:
                    system_state_severity = 3
                    lg([this_t,
                        "cell " + str(cell_id) + "->" + str(voltage_p_cell[cell_id]) + ' below ' + str(
                            drone_variables['cell_min_v'])], 1)

        if current_time - system_state['high']['threads']['current_data_read'] > 2000:
            system_state_severity = 3
            lg([this_t, "thread current_data_read is dead"], 1)
        elif drone_variables['allow_bat_ah'] < current['total']:
            system_state_severity = 3
            lg([this_t, "used batt ah " + str(current['total']) + " > " + str(drone_variables['allow_bat_ah'])], 1)

        # thread cant send any comands to pixhawk, marking severity 3 makes no affect, but lets do it anyway
        if current_time - system_state['high']['threads']['send_drone_commands'] > 2000:
            system_state_severity = 3

        #################### system severity validation and evaluation ##########################

        # if pixhawk last response > 3s, send_drone_commands is dead, try inserting cmd directly
        if current_time - pixhawk_state['last_response'] > 3000 and pixhawk_state['state'] == 'armed':
            lg([this_t, 'pixhawk last_response < 3s, try sending RTL directly to flight_procedures'], 1)
            set_drone_mode_state('rtl', flight_procedures)
            command_send_time = current_time

        # got system state severity. take action if :
        # 1. we executed action long time ago (>20000ms)
        # 2. and drone is airborn
        ##### 3. pixhawk is responding (does this last statement make any sense?)
        elif system_state_severity \
                and current_time - command_send_time > 20000 \
                and pixhawk_state['system_state'] not in ["standby", None]:

            if system_state_severity == 1 and \
               pixhawk_state["vehicle_mode"] not in ['rtl', 'land']:  # if we already doing severity 2 commands

                lg([this_t, "system severity fixable(1)"], 3)
                drone_commands.append('go_home_position')

            if system_state_severity == 2 and \
               pixhawk_state["vehicle_mode"] not in ['rtl', 'land']:  # if we already doing severity 2 commands

                lg([this_t, "system severity severe(2)"], 2)
                drone_commands.append('rtl')

            if system_state_severity == 3 and \
               pixhawk_state["vehicle_mode"] not in ['land']:  # if we already doing severity 3 cmd

                lg([this_t, "system severity critical(3)"], 1)
                drone_commands.append('land')
            command_send_time = current_time

        time.sleep(2)

    time.sleep(1)
    lg([this_t, "THREAD done"], 2)


def main():
    # print command line arguments
    # for arg in sys.argv[1:]:
    #     print(arg)
    global cycling
    global socket
    global reset_total_amps
    global terminate_all
    global transmit

    for port in serial.tools.list_ports.comports():
        print("available USB ports hwid:" + port.hwid)

    getDroneVariables()
    lg("Connecting to ZMQ server", 2)
    socket.connect(zmq_server_string)

    try:
        Thread(target=watch_rc_inputs).start()
    except:
        lg("Error: unable to start watch_rc_inputs thread", 1)

    # start thread to watch if connection to station is ok
    try:
        Thread(target=station_connection).start()
    except:
        lg("Error: unable to start station_connection thread", 1)

    try:
        Thread(target=send_drone_commands, args=(e_start_now,)).start()
    except:
        lg("Error: unable to start drone commands thread", 1)

    try:
        if drone_variables['dummy_voltage'] > 0:
            Thread(target=voltage_data_read_dummy).start()
        else:
            Thread(target=voltage_data_read).start()
    except:
        lg("Error: unable to start voltage thread", 1)

    try:
        Thread(target=current_data_read).start()
    except:
        lg("Error: unable to start voltage thread", 1)

    try:
        Thread(target=start_video_transmission).start()
    except:
        lg("Error: unable to start start_video_transmission", 1)

    try:
        Thread(target=camera_angle_controll_Main, args=(e_start_now_camera_Master,)).start()
    except:
        lg("Error: unable to start t_start_camera_controll_Main", 1)

    try:
        Thread(target=geo_fencing).start()
    except:
        lg("Error: unable to start geo_fencing", 1)

    try:
        Thread(target=network_state_daemon).start()
    except:
        lg("Error: unable to start network_state_daemon", 1)

    try:
        Thread(target=system_error_state_handler).start()
    except:
        lg("Error: unable to start system_error_state_handler, terminating application", 1)
        cycling = False
        time.sleep(2)

    this_t = 'MAIN_cycling'
    while cycling:
        #  if request is unknown, reply with message
        lg([this_t, "cycling"], 2)
        got_invalid_msg = True

        server_message = socket.recv_json()
        lg([this_t, "MSG <- station: " + str(server_message)], 2)

        message = json.loads(server_message)

        station_state['last_response'] = int(time.time() * 1000)

        # these drone commands are not in hight priority to execute
        if message['msg'] in ['arm', 'take_off', 'get_rc_ch', 'get_state', 'get_drone_gps']:
            lg([this_t, "got " + message['msg']], 3)
            got_invalid_msg = False
            drone_commands.append(message['msg'])

        # these commands are in hight priority to execute
        if message['msg'] in ['disarm', 'land', 'stab', 'loiter', 'set_rc_ch', 'rtl', 'guided',
                              'poshold', 'man', 'dig']:
            lg([this_t, "got " + message['msg']], 3)
            got_invalid_msg = False
            drone_commands.append(message['msg'])
            e_start_now.set()

        if (message['msg']).startswith('rc_1234'):
            lg([this_t, "got " + message['msg']], 3)
            got_invalid_msg = False
            drone_commands.append(message['msg'])
            e_start_now.set()
            set_camera_pitch_angle(message['msg'])

        # if (message['msg']).startswith('set_station_gps'):
        #     lg([this_t, "got " + message['msg']], 3)
        #     got_invalid_msg = False

        if message['msg'] == '9':
            lg([this_t, "got " + message['msg']], 2)
            cycling = False
            got_invalid_msg = False
            lg([this_t,
                '====VERBOSE==IMPORTANT=========== got 9 msg, quiting application ========VERBOSE==IMPORTANT===='], 1)

        if message['msg'] == 'reset':
            lg([this_t, "got " + message['msg']], 2)
            reset_total_amps = True
            got_invalid_msg = False

        if message['msg'] == 'ping':
            lg([this_t, "got " + message['msg']], 3)
            response_list.append({'heartbeat': 'pong'})
            got_invalid_msg = False

        if message['msg'] == 'vid':
            lg([this_t, "got " + message['msg']], 3)
            transmit = True
            got_invalid_msg = False

        if got_invalid_msg:
            lg([this_t, "got unrecognizable msg " + message['msg']], 1)
            response_list.append({'msg': 'not recognized'})

        reply_all_data()

    time.sleep(1)
    terminate_all = True
    time.sleep(3)
    lg([this_t, "THREAD MAIN done "], 2)


if __name__ == "__main__":
    main()
